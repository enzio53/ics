<?php
error_reporting(0);

require __DIR__ . '/vendor/autoload.php';

function SPLRegister() {
    $ics = new stdClass();
    $data = [];
    $xml = simplexml_load_file(__DIR__ . '/external.xml');

    foreach ($xml->{'automatically-loaded'}->file as $file) {
        if ($file) {
            require $file;
        }
    }

    foreach ($xml->{'routing-list'}->route as $route) {
        $data[(string) $route->attributes()->location] = (string) $route->attributes()->file;
    }

    return [(object) $ics, $data];
}

function SPLFinish($ics) {
    return (array) $ics;
}

$register = SPLRegister();
$data = $register[1];
$ics = $register[0];
$ics = SPLFinish($ics);

//print_r($ics['router']['register'](['test' => 'debug']));
echo $ics['communication']['open']('hi');
$ics['router']['register']($data);
