<?php
$config = [
    'module-name' => 'ICS / SPL Communication',
    'module-version' => 'communication',

    'folders' => [
        'module-data' => '/data',
        'module-save' => '/communication',
    ],
];

$version = $config['module-version'];

# We write all functions for $ics
$ics->$version->open = function ($client) use ($config) {
    $folders = $config['folders'];
    $path = __DIR__ . $folders['module-data'] . $folders['module-save'] . '/' . crc32($client) . '.dat';

    if (is_dir(__DIR__ . $folders['module-data'])) {
        if (is_dir(__DIR__ . $folders['module-data'] . $folders['module-save'])) {
            if (!file_exists($path)) {
                $handle = fopen($path, 'x+');
                fwrite($handle, json_encode(['ready' => true]));
                fclose($handle);
            } else {
                echo 'ready';
            }
        } else {
            if (!mkdir(__DIR__ . $folders['module-data'] . $folders['module-save'], 0777, true)) {
                die('Permission error');
            }
        }
    } else {
        if (!mkdir(__DIR__ . $folders['module-data'], 0777, true)) {
            die('Permission error');
        }
    }
};

# We close the object and transform in array
if (is_object($ics->$version)) {
    $ics->$version->errors = false;
    $ics->$version = (array) $ics->$version;
}