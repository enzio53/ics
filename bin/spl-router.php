<?php
$router = new \Bramus\Router\Router();
$config = [
    'module-name' => 'ICS Router',
    'module-version' => 'router',
];

$version = $config['module-version'];

$ics->$version->register = function ($route) use ($router, $config) {
    if (is_array($route)) {
        $config += ['0x' => $route];
    }

    foreach ($config['0x'] as $key => $value) {
        $router->get($key, function() use ($value){
            echo $value;
        });
    }

    $router->run();
};

# polish result
if (is_object($ics->$version)) {
    $ics->$version->errors = false;
    $ics->$version = (array) $ics->$version;
}